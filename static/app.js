

(function () {
  'use strict';

  var app=angular
    .module('myApp', [
      //'myApp.config',

      'ngAnimate',
      'myApp.search',
      'myApp.primary',
      'myApp.authentication',
      'myApp.routes',
      'myApp.symptoms',
      //'myApp.conditions'
      'myApp.conditions',
      'myApp.medications',
      'myApp.procedures',
      'myApp.doctors',
      'myApp.hospitals',
      'myApp.dashboard',
      'myApp.WhereCouldIGo',

      'myApp.publicService',
      'myApp.scrollService',
      'myApp.cacheService',
      'myApp.authService',
      'myApp.geoService',
      'perfect_scrollbar',
      'uiSwitch',
      /*'ngSlider',*/
      /*'ui.checkbox',*/
      'ngMaterial',
      'ngMdIcons',
      'youtube-embed',
      'facebook',
      'customDirectiveApp',
      '720kb.socialshare',
      'ngClipboard',




      /*'headroom'*/


     // 'thinkster.accounts',
     // 'thinkster.authentication',
      //'thinkster.layout',
      //'thinkster.posts',
      //'thinkster.utils'
    ]);

    app.config(['FacebookProvider', function(FacebookProvider, $provide){
          var myAppId = '1621738971389938';
          FacebookProvider.init(myAppId);
    }]);



    //***********For set Focus on Searching Area*********
    app.directive("focus", function($state){
      var inter;
      return{
        restrict:'A',
        link:function(scope, elem, attr)
        {
          //angular.element(elem).focus();
          
        }
      }
    });

    //***********For Doctor Focus*********
    app.directive("doctorFocus", function($state){
      var inter;
      return{
        restrict:'A',
        link:function(scope, elem, attr)
        {
          elem.bind("mouseover", function(){
            elem.addClass("doctors_mouseover");
          });
          elem.bind("mouseout", function(){
            elem.removeClass("doctors_mouseover");
          });
          elem.bind("click", function(){
            $(".doctors_mouseclick").removeClass("doctors_mouseclick");
            elem.addClass("doctors_mouseclick");
          });

        }
      }
    });

    //**********Click outside of Tag then automatically Hidden field*******
    app.directive("clickOutSideTag", function(){
        return {
            restroct : 'A',
            link : function(scope, elem, attr)
            {
                elem.bind("click", function(e){
                    elem.addClass("devsite-popout-closed");
                    console.log(elem.hasClass("devsite-popout-closed"));

                });
                angular.element("body").bind("click", function(){
                    elem.removeClass("devsite-popout-closed");

                });
            }
        }
    });
    //**********For Check login or not if click any Overview left side Icon Click*****
    app.directive("checkLogin", function($timeout, CacheService, $rootScope){
      return{
        restrict : 'A',
        link : function(scope, elem, attr){
          elem.bind("click", function(event){
            if(!CacheService.isCache("client_info"))
            {
              $timeout(function(){
                $('#tile1 .loginClickClass').click();
              });
              event.preventDefault();
              event.stopPropagation();
            }
            /*else
            {
                attr.$observe('checkLogin', function(value) {
                  elem.html('<img src="'+$rootScope.static_path+'lib/image/icons/filled-star.svg"/>');
                });
            }*/
          });
        }
      }
    });

    //***************For Dynamically load Image for image mapster******
    app.directive('imageonload', function($timeout, $state, $rootScope) {
    return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                element.bind('load', function() {
                  scope.$apply(function(){
                    scope.photo.isLoading=true;
                  });
                    $('#avatar_img').mapster({
                      mapKey: 'avatar-key',
                      render_select: {
                         altImage: $rootScope.static_path + 'lib/image/avatar/male_front_sec.png',
                         altImageOpacity: 1
                      },
                      singleSelect: true,
                      fillOpacity: 0.2,
                      fillColor: 'ffffff',

                      onClick:function(e){
                          $state.go($(this).attr("state-name"), {avatar_id:e.key, avatar_code:$(this).attr("avatar-code")});
                        }
                    });
                });
            }
        };
    });

    //************For go to Top of container****
    app.directive('goToTop', function(ScrollService) {
    return {
            restrict: 'A',
            scope : {
              goToTop : "@"
            },
            link: function(scope, element, attrs) {
                element.bind('click', function() {
                  ScrollService.scrollTop(scope.goToTop);
                });
            }
        };
    });

    //************Check User Location if get then then show Specility relate to current Specility*****
    app.directive('userLocationCheck', function(CacheService, $rootScope) {
    return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                element.bind('click', function(event) {
                  if(!CacheService.isCache("user_location"))
                  {
                    scope.$apply(function(){
                      $rootScope.isUserLocationFind=false;
                    });
                    event.preventDefault();
                    event.stopPropagation();
                  }
                });
            }
        };
    });

    //****** for User Location Location Template & it's Functionlity********
    app.directive('userLocationTemplate', function($rootScope, GeoLocationService, CacheService) {
    return {
            restrict: 'EA',
            template : '<div layout="row" style="box-shadow:0.1px 0.1px 5px rgb(171, 171, 171)"><span flex="80"><md-button style="color:white">Please specify your location</md-button></span><span flex="20"><md-button ng-click="findMyCurrentLocation()"><md-icon md-svg-icon="{{static_path}}lib/image/location_icon/location.svg" class="animate-location-icon"></md-icon><md-tooltip>Get Current Location</md-tooltip></md-button></span></div><div layout="column"><form name="geo_form"><md-input-container><label style="color: white;border-color: rgba(245, 31, 31, 0.12); margin-left:10px">City* (e.g. Delhi)</label><input type="text" required style="color:white" name="geo_city" ng-model="user_city" ng-focus="getCityList()"></md-input-container><div style="width:98%; height:200px; background:rgb(40, 40, 40); margin-top:-25px; overflow:auto" ng-class="({city_list_display_hide:isCityListDisplay, city_list_display_show:!isCityListDisplay})"><md-list class="list_view"><md-list-item ng-repeat="city in cityList | filter:user_city" ng-click="setUserCity(city.name, city.slug)"><md-button>{{city.name}}</md-button></md-list-item><md-list-item style="color:green" ng-show="isShowAllCities" ng-init="isShowAllCities=true"><a ng-click="showAllCities(); isShowAllCities=false">Other Cities</a></md-list-item></md-list></div><md-input-container><label style="color: white;border-color: rgba(245, 31, 31, 0.12); margin-left:10px">Location(e.g. Preet Vihar) (optional)</label><input type="text" style="color:white" ng-model="user_location" ng-change="getLocalityList()" ng-disabled="geo_form.geo_city.$error.required"></md-input-container><div style="width:98%; height:200px; background:rgb(40, 40, 40); margin-top:-25px; overflow:auto" ng-class="({locality_list_display_hide:isLocalityListDisplay, locality_list_display_show:!isLocalityListDisplay})"><md-list class="list_view"><md-list-item ng-repeat="locality in localityList" ng-click="setUserLocality(locality.name, locality.slug)"><md-button>{{locality.name}}</md-button><md-list-item></md-list></div><md-button class="md-raised md-primary" style="width:100%; display: inline-block!important; opacity: 1!important; top: -0px!important;" ng-click="findMyLocation()" ng-disabled="geo_form.geo_city.$error.required">Set Location</md-button><md-progress-linear md-mode="indeterminate" ng-show="isSearching"></md-progress-linear></form></div>',
            link: function(scope, element, attrs) {

                scope.isCityListDisplay=true;
                scope.isLocalityListDisplay=true;
                scope.cityList = [];

                var city_slug=null, location_slug=null;
                //********* Get Geo Location********
                scope.findMyCurrentLocation=function()
                {
                  scope.isSearching=true;
                  scope.$$listeners['GeoLocationEvent'] = [];
                  GeoLocationService.getCurrentLocation();
                  scope.$on("GeoLocationEvent", function($event, data){
                      
                      scope.isSearching=false;
                      scope.$apply(function(){
                          scope.user_city=data.city;
                          scope.user_location=data.locality;
                      });
                  });
                }

                //********* Set User Location When Click Set Location Button********
                scope.findMyLocation=function(){
                    var location_={
                        city : scope.user_city,
                        location : scope.user_location
                    }
                    
                    CacheService.setCache("user_location", {user_location : location_.location, user_location_slug : location_slug, user_city : location_.city, user_city_slug :city_slug});
                    $rootScope.isUserLocationFind=true;
                    $rootScope.$broadcast("LocationChangesEvent");
                }

                //********* Get City List From MySwaasth Database********
                scope.getCityList = function()
                {
                  scope.isLocalityListDisplay=true;
                  if(scope.isCityListDisplay==true)
                  {
                    if(CacheService.isCache("myswaasth_city_list"))
                    {
                      var m_city = (CacheService.getCache("myswaasth_city_list")[1]);
                      scope.cityList = m_city.major_cities;
                      scope.isCityListDisplay=false;
                    }
                    else
                    {
                      GeoLocationService.getCityList().success(function(response){
                        scope.cityList = response[1].major_cities;
                        CacheService.setCache("myswaasth_city_list", response);
                        scope.isCityListDisplay=false;
                      }).error(function(error){
                        $rootScope.alerts[0]={ type: 'danger', msg: 'Not Get City List. May be Internet Connection Failed.' };
                      });
                    }
                  }
                }

                scope.setUserCity = function(name, slug)
                {
                  scope.user_city=name;
                  city_slug = slug;
                  scope.isCityListDisplay=true;
                }

                scope.showAllCities = function()
                {
                  scope.cityList = CacheService.getCache("myswaasth_city_list")[0].cities;
                }
                //********* Get Locality List from MySwaasth Database********
                scope.getLocalityList = function()
                {
                  scope.isCityListDisplay=true;
                  GeoLocationService.getLocalityList(city_slug, scope.user_location).success(function(response){
                    scope.localityList = response;
                    scope.isLocalityListDisplay=false;
                  }).error(function(error){
                    $rootScope.alerts[0]={ type: 'danger', msg: 'Not Get Locality List. May be Internet Connection Failed.' };
                  });
                }

                scope.setUserLocality = function(name, slug)
                {
                  scope.user_location=name;
                  location_slug = slug;
                  scope.isLocalityListDisplay=true;
                }

            }
        };
    });

    //********* Show Loader when image loading******
    app.directive('imageLoading', function() {
    return {
            restrict: 'A',
            link: function(scope, ele, attrs) {
              ele.bind("progress", function(e){
                console.log("progress");
              });
              ele.bind("load", function(e){
                scope.$apply(function(){
                  scope.photo.isLoading=true;
                });
              });
            }
        };
    });

    //********** Date Picker Directive*******
    app.directive('datePicker', function() {
    return {
            restrict: 'A',
            link: function(scope, ele, attrs) {
              $(ele).datepicker({dateFormat: 'yy-mm-dd'});
            }
        };
    });
    //***********For Clear all HTML data with in JSON*********
    app.filter("plainHTMLText", function(){

      return function(text)
      {
        return text.replace(/<[^>]+>/gm, '');
      }
    });

    app.filter("FilterList", function(){

      return function(input, filter_txt)
      {
        if(filter_txt=="")
        {
          return input;
        }
        else
        {
          if(input.slice(0, filter_txt.length)==filter_txt)
          {
            return input;
          }
        }
      }
    });

    //************ Filter for Parse Float to Integer***********
    app.filter("toInt", function(){

      return function(num)
      {
        return parseInt(num);
      }
    });


  angular.module('myApp.config', []);

  angular.module('myApp.routes', ['ngRoute', 'ui.router','infinite-scroll', 'ngStorage', 'ui.bootstrap', 'ngSanitize', 'base64', 'directive.g+signin']).run(function($rootScope, $location, $state, ScrollService) {
      $rootScope.alerts = [
            
            ];

      $rootScope.closeAlert = function(index) {
        $rootScope.alerts.splice(index, 1);
      };
      $rootScope.avataranim = 'change';

      $rootScope.location=$location;

      $rootScope.isUserLocationFind=false;
      /*$rootScope.static_path = 'https://healthbrio.s3.amazonaws.com/';*/
      $rootScope.static_path = 'http://127.0.0.1:8000/static/';
      $rootScope.static_server_path = 'http://52.74.163.60/';

      $rootScope.goToLeft=function(){
        ScrollService.scrollBack();
      }
      $rootScope.goToRight=function(){
        ScrollService.scrollBackword();
      }
      //***********Native button Click coding********
      /*$rootScope.$on('$locationChangeSuccess', function() {
        $rootScope.actualLocation = $location.path();
      });
      $rootScope.$watch(function () {return $location.path()}, function (newLocation, oldLocation) {
        if($rootScope.actualLocation === newLocation) {
            var state_name=$state.current.name;
            if(state_name=="symptoms" || state_name=="conditions" || state_name=="medications" || state_name=="procedures" || state_name=="hospitals" || state_name=="doctors")
            {
              $state.reload();
            }
        }
      });*/
  });

})();






