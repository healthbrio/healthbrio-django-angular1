/**
 * Created by IDCS12 on 6/4/2015.
 */
/**
 * Created by IDCS12 on 6/4/2015.
 */
(function(){
    'use strict';

    angular.module('myApp.dashboard.myprofile.controller')
        .controller('MyprofileController', MyprofileControllerFun);

    angular.module('myApp.dashboard.myprofile.controller')
        .controller('MyprofileoverviewController', MyprofileoverviewControllerFun);

    angular.module('myApp.dashboard.myprofile.controller')
        .controller('changePasswordController', changePasswordControllerFun);

    MyprofileControllerFun.$inject = ['$scope', '$http', '$stateParams', '$timeout', '$state', '$window', 'MyProfileService', 'ScrollService'];
    MyprofileoverviewControllerFun.$inject = ['$scope', '$filter', '$http', '$stateParams', '$timeout', '$state', '$window', '$rootScope', 'MyProfileOverviewService', 'ScrollService'];
    changePasswordControllerFun.$inject = ['$scope', '$http', '$stateParams', '$timeout', '$state', '$window', 'ScrollService', 'AuthService', 'CacheService'];


    function MyprofileControllerFun($scope, $http, $stateParams, $timeout, $state, $window, MyProfileService, ScrollService){
        var vm = this;
        vm.isLoading=true;

        vm.myProfileList = [];
        vm.windowHeight = $window.innerHeight;
        vm.windowHeight = vm.windowHeight - 171;

        vm.viewheight = {
            'height': vm.windowHeight + "px"
        };
        
        MyProfileService.all().success(function(response){
            vm.myProfileList=response;
            vm.isLoading=false;
        }).error(function(error){
            alert(error.detail);
            vm.isLoading=false;
        });

        $scope.$on("MyProfileUpdateEvent", function(event){
            MyProfileService.all().success(function(response){
                vm.myProfileList=response;
                vm.isLoading=false;
            }).error(function(error){
                alert(error.detail);
                vm.isLoading=false;
            });
        });
        vm.closeView=function(){
            $state.go("dashboard");
            ScrollService.scrollBack();
        };
        ScrollService.scrollStart();
    }

    function MyprofileoverviewControllerFun($scope, $filter, $http, $stateParams, $timeout, $state, $window, $rootScope, MyProfileOverviewService, ScrollService){

        var vm = this;
        vm.isLoading=true;
        /*vm.myProfile;*/
        vm.isNewMamber=false;
        var d=new Date();
         vm.myProfileList = [];
        vm.windowHeight = $window.innerHeight;
        vm.windowHeight = vm.windowHeight - 171;

        vm.viewheight = {
            'height': vm.windowHeight + "px"
        };


        vm.profile={
            profile_fname : null,
            profile_lname : null,
            profile_gender : null,
            profile_dob : null,
            profile_mobile : null,
            profile_email : null,
            profile_postalcode : null,
            profile_isSuperUser : null,
            today : d.getFullYear()+"-"+(d.getMonth()>9?(d.getMonth()+1):"0"+(d.getMonth()+1))+"-"+(d.getDate()>9?d.getDate():"0"+d.getDate())
        };

        if($stateParams.profile_id!="addNewMamber")
        {
            MyProfileOverviewService.all($stateParams.profile_id).success(function(response){


                /*vm.myProfile=response;*/
                vm.profile.profile_isSuperUser=response.is_super;

                vm.profile.profile_fname=response.first_name;
                vm.profile.profile_lname=response.last_name;
                vm.profile.profile_gender=response.gender;
                vm.profile.profile_dob = response.date;
                vm.profile.profile_mobile=response.mobile;
                vm.profile.profile_email=response.email;
                vm.profile.profile_postalcode=response.zipcode;
                vm.isLoading=false;

            }).error(function(error){
                alert(error.detail);
                vm.isLoading=false;
            });
        }
        else
        {
            vm.isLoading=false;
            vm.isNewMamber=true;
            vm.profile.profile_gender="M";
        }
        

        vm.saveDetails=function(){

            vm.isLoading=true;
            MyProfileOverviewService.save(vm.profile).success(function(response){

                vm.isLoading=false;
                $rootScope.$broadcast("MyProfileUpdateEvent");
                $state.go("dashboard.myprofile");
                $scope.alerts[0]={ type: 'success', msg: 'Profile Successfully Saved' };

            }).error(function(error){
                $scope.alerts[0]={ type: 'danger', msg: 'Profile Not Saved' };
                vm.isLoading=false;
            });
        };
        vm.cancelDetails=function(){
            
        };
        vm.deleteDetails=function(){
            vm.isLoading = true;
            MyProfileOverviewService.remove($stateParams.profile_id).success(function(response){
                vm.isLoading=false;
                $rootScope.$broadcast("MyProfileUpdateEvent");
                $state.go("dashboard.myprofile");
                $scope.alerts[0]={ type: 'success', msg: 'Successfully Deleted' };

            }).error(function(error){
                $scope.alerts[0]={ type: 'danger', msg: 'Profile Not Deleted' };
                vm.isLoading=false;
            });
        };
        vm.updateDetails=function()
        {
            vm.isLoading=true;
            if(vm.profile.profile_postalcode=="")
            {
                vm.profile.profile_postalcode=null;
            }
            MyProfileOverviewService.update(vm.profile, $stateParams.profile_id).success(function(response){

                vm.isLoading=false;
                $rootScope.$broadcast("MyProfileUpdateEvent");
                $state.go("dashboard.myprofile");
                $scope.alerts[0]={ type: 'success', msg: 'Successfully Updated' };

            }).error(function(error){
                $scope.alerts[0]={ type: 'danger', msg: 'Profile not Updated' };
                vm.isLoading=false;
            });
        };
        vm.openInputFile =  function(){
            angular.element('#file-id').trigger('click');
        };
        vm.closeView=function(){
            $state.go("dashboard.myprofile");
            ScrollService.scrollBack();
        };
        ScrollService.scrollStart();
    }

    function changePasswordControllerFun($scope, $http, $stateParams, $timeout, $state, $window, ScrollService, AuthService, CacheService){
        var vm = this;
        vm.isLoading=false;
        vm.windowHeight = $window.innerHeight;
        vm.windowHeight = vm.windowHeight - 171;

        vm.viewheight = {
            'height': vm.windowHeight + "px"
        };
        vm.changePass=function()
        {
          vm.isLoading=true;
          AuthService.changePassword(vm.old_pass, vm.new_pass).success(function(response){
                $scope.alerts[0]={ type: 'success', msg: 'Successfully Changed Password' };
                $state.go("dashboard");
                vm.isLoading=false;
            }).error(function(error){
                $scope.alerts[0]={ type: 'danger', msg: 'Password Not Changed' };
                vm.isLoading=false;
            });;
        }

        vm.closeView=function(){
            $state.go("dashboard");
            ScrollService.scrollBack();
        };
        ScrollService.scrollStart();
    }

})();