/**
 * Created by IDCS12 on 6/4/2015.
 */
(function(){
    'use strict';
    var myprofile_service=angular.module('myApp.dashboard.myprofile.services');
    myprofile_service.factory('MyProfileService', myProfileServiceFun);
    myprofile_service.factory('MyProfileOverviewService', myProfileOverviewServiceFun);

    myProfileServiceFun.$inject=['HttpService'];
    myProfileOverviewServiceFun.$inject=['HttpService', '$filter'];
    
    function myProfileServiceFun(HttpService)
    {
        var myProfileApiSerivce  = {
                all : all
            }

            function all(){
                
                return (HttpService.PrivateServiceURL('client/patient_myprofile_list/', 'GET'));
            }

        return myProfileApiSerivce;
    }

    function myProfileOverviewServiceFun(HttpService, $filter)
    {
        var myProfileApiSerivce  = {
            all : all,
            save : save,
            remove : remove,
            update : update
        };



        function all(profile_id){
            return (HttpService.PrivateServiceURL('client/patient_myprofile_detail/'+profile_id, 'GET'));
        }
        function save(profile){
            var data={
                    "id": 10,
                    "patient": "Lalit Singh",
                    "first_name" : profile.profile_fname,
                    "last_name" : profile.profile_lname,
                    "gender" : profile.profile_gender,
                    "date" : $filter('date')(profile.profile_dob, "yyyy-MM-dd"),
                    "email" : profile.profile_email,
                    "mobile" : profile.profile_mobile,
                    "zipcode" : profile.profile_postalcode,
                    "is_super" : false
                };
            return (HttpService.PostPrivateServiceURL('client/patient_myprofile_list/', data, 'POST'));
        }
        function remove(profile_id)
        {
            return (HttpService.PrivateServiceURL('client/patient_myprofile_detail/'+profile_id, 'DELETE'));
        }
        function update(profile, profile_id)
        {
            var data={
                    "id": 10,
                    "patient": "Lalit Singh",
                    "first_name" : profile.profile_fname,
                    "last_name" : profile.profile_lname,
                    "gender" : profile.profile_gender,
                    "date" : $filter('date')(profile.profile_dob, "yyyy-MM-dd"),
                    "mobile" : profile.profile_mobile,
                    "email" : profile.profile_email,
                    "zipcode" : profile.profile_postalcode,
                    "is_super" : false
                };
            return (HttpService.PostPrivateServiceURL('client/patient_myprofile_detail/'+profile_id, data, 'PUT'));
        }


        return myProfileApiSerivce;
    }
})();