/**
 * Created by IDCS12 on 3/18/2015.
 */
(function(){
    angular.module('myApp.procedures.controller')
        .controller('ProceduresController', ProceduresControllerFun);
    angular.module('myApp.procedures.controller')
        .controller('ProceduresOverviewController', ProceduresOverviewControllerFun);
    
    ProceduresControllerFun.$inject = ['$scope','$state', '$http', '$interval', '$timeout', '$window', 'ProceduresService', 'ScrollService', 'CacheService'];
    ProceduresOverviewControllerFun.$inject = ['$scope','$http', '$stateParams', '$state', '$window', 'ScrollService', 'CacheService', 'ProceduresOverview','$mdDialog' ,'$rootScope'];

    function ProceduresControllerFun($scope, $state, $http, $interval, $timeout, $window, ProceduresService, ScrollService, CacheService)
    {
        var vm=this;
        vm.procedureList=[];

        vm.isSearch_box = false;
        vm.isDisabled=false;
        vm.isLoading=false;

        vm.busy = false;

        vm.after = 0;

        vm.countover = true;
        var isloaded=true;
        vm.isFilter=false;
        vm.windowHeight = $window.innerHeight;
        vm.windowHeight = vm.windowHeight - 171 - 65;

        vm.viewheight = {
            'height': vm.windowHeight + "px"
        };

        /*if (CacheService.isCache("procedures_cache")) {
            var data = CacheService.getCache("procedures_cache");
            
            isloaded=false;
            var i=0;
            vm.isLoading=true;

            $timeout(function(){
                vm.isLoading=false;
                var interval=$interval(function(){

                    vm.procedureList.push(data.list[i]);
                    i++;
                    
                    if(i==data.list.length)
                    {
                        isloaded=true;
                        $interval.cancel(interval);
                    }
                
                }, 10);
            }, 1500);
            
            vm.after = data.after;
            window.isRefresh = false;
            vm.busy = false;
        }*/
         
        vm.loadMore = function () {
            
            if (CacheService.isCache("procedures_cache")) {
                var data = CacheService.getCache("procedures_cache");
                
                if(data.after>vm.after)
                {
                    isloaded=false;
                    for(var i=vm.after; i<(vm.after+20); i++)
                    {
                        if(data.list[i]!=null)
                        {
                            vm.procedureList.push(data.list[i]);
                        }
                    }
                    vm.after+=20;
                    if(data.after<=vm.after)
                    {
                        vm.after = data.after;
                        isloaded=true;
                    }
                    vm.busy = false;
                }

                if(vm.after>data.total)
                {
                    vm.countover=false;
                }
            }
            
            if (vm.busy || !isloaded) return;
            if(vm.isFilter) return;

            //var url = "http://52.74.163.60/api/symptoms_list/?offset="+vm.after;
            if (vm.countover) {
                vm.busy = true;
                ProceduresService.all(vm.after).success(function (data) {
                    var items = data.results;
                    for (var i = 0; i < items.length; i++) {
                        vm.procedureList.push(items[i]);
                    }
                   
                    vm.after = vm.after + 20;
                    var data1 = {
                        list: vm.procedureList,
                        after: vm.after,
                        total : data.count
                    };
                    
                    CacheService.setCache("procedures_cache", data1);

                    vm.busy = false;
                    console.log("after value is " + vm.after);
                    if (vm.after >= data.count) {
                        console.log("false countover");
                        vm.countover = false;
                    } else {
                        console.log("true countover ");
                    }
                }.bind(this)).error(function () {
                    vm.busy = false;
                    $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
                });
            }
        }

        vm.searchButton = function () {
                vm.isSearch_box = !vm.isSearch_box;
            }
        
        var timeout, interval;
        vm.filterListChange=function(filterList)
        {

            vm.isFilter=true;
            vm.procedureList=[];

            if(filterList!="")
            {
                $timeout.cancel(timeout);
                $interval.cancel(interval);
                vm.isLoading=true;
                timeout=$timeout(function(){
                    vm.isDisabled=true;
                    vm.isFilter=true;
                    ProceduresService.filterService(filterList).success(function (response) {
                        vm.procedureList=response.results;
                        vm.isDisabled=false;
                        vm.isLoading=false;
                    }).error(function(error){
                        $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
                        vm.isDisabled=false;
                        vm.isLoading=false;
                    });
                }, 500);
            }

            if(filterList=="")
            {

                $timeout.cancel(timeout);
                vm.isLoading=false;
                if (CacheService.isCache("procedures_cache")) {
                var data = CacheService.getCache("procedures_cache");
                isloaded=false;
                var i=0;
                interval=$interval(function(){

                    vm.procedureList.push(data.list[i]);
                    i++;
                    
                    if(i==data.list.length)
                    {
                        isloaded=true;
                        vm.isFilter=false;
                        $interval.cancel(interval);
                    }
                    
                }, 10);
                vm.after = data.after;

                }
            }
        }

        ScrollService.scrollBegin();
    }
    
    function ProceduresOverviewControllerFun($scope, $http, $stateParams, $state, $window, ScrollService, CacheService, ProceduresOverview,$mdDialog, $rootScope)
    {
        
        var vm=this;
        vm.title=$stateParams.procedure_name;
        vm.proceduresOveriew=[];
        vm.isLoading=true;
        vm.alert = '';
        vm.index=0;

        vm.myProcedureId=0;
        vm.isStarFill=false;

        vm.viewheight = {
            height : ($window.innerHeight-171)+"px"
        }
        
        if(CacheService.isCache("procedures_"+$stateParams.procedure_id))
        {
            //***Search sessionStorage when click any Cause
            vm.proceduresOveriew=CacheService.getCache("procedures_"+$stateParams.procedure_id);
            vm.title=vm.proceduresOveriew[0].name;
            vm.isLoading=false;
            //alert("from cache");
        }
        else
        {
            //alert("from api");
            //***get Data from API and store it sessionStorage
            ProceduresOverview.all($stateParams).success(function(response){
                
                vm.proceduresOveriew = response;
                vm.title=vm.proceduresOveriew[0].name;
                CacheService.setCache("procedures_"+$stateParams.procedure_id, response);
                vm.isLoading=false;
              }).error(function(error){
                    $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
            });
        }


        vm.closeView=function(){
            
            $state.go($state.current.name.substring(0, $state.current.name.indexOf(".procedures_overview")));
            ScrollService.scrollBack();
        }

        
        //*********for Display Popup Video and Images
        vm.showImage = function(ev, index) {

            vm.index=index;
            $mdDialog.show({
              controller: DialogController,
              templateUrl: $rootScope.static_path +'templates/imgvideodialog/dialog1.img.html',
              targetEvent: ev
            })
            .then(function(answer) {
              $scope.alert = 'You said the information was "' + answer + '".';
            }, function() {
              $scope.alert = 'You cancelled the dialog.';
            });
          };

        function DialogController($scope, $mdDialog) {
           // Set of Photos
            $scope.static_path = window.static_path;
            $scope.photos = [];
            for(var n=0; n<vm.proceduresOveriew[0].image.length; n++)
            {
                $scope.photos.push({src: vm.proceduresOveriew[0].image[n].desktop_image, desc: vm.proceduresOveriew[0].image[n].title, isLoading:false});
            }
            

            // initial image index
            $scope._Index = vm.index;

            // if a current image is the same as requested image
            $scope.isActive = function (index) {

                return $scope._Index === index;
            };

            // show prev image
            $scope.showPrev = function () {
                $scope._Index = ($scope._Index > 0) ? --$scope._Index : $scope.photos.length - 1;
            };

            // show next image
            $scope.showNext = function () {
                $scope._Index = ($scope._Index < $scope.photos.length - 1) ? ++$scope._Index : 0;
            };

            // show a certain image
            $scope.showPhoto = function (index) {
                $scope._Index = index;
            };

          $scope.answer = function(answer) {
            $mdDialog.hide(answer);
          };
          

        }

        vm.showVideo = function(ev, index) {

            vm.index=index;
            $mdDialog.show({
              controller: VideoController,
              templateUrl: $rootScope.static_path+ 'templates/imgvideodialog/dialog1.video.html',
              targetEvent: ev
            })
            .then(function(answer) {
              $scope.alert = 'You said the information was "' + answer + '".';
            }, function() {
              $scope.alert = 'You cancelled the dialog.';
            });
          };

        function VideoController($scope, $mdDialog) {
           // Set of Photos
            $scope.static_path = window.static_path;
            $scope.videos = [];
            $scope.videosthumbs = [];
            for(var n=0; n<vm.proceduresOveriew[0].video.length; n++)
            {
                $scope.videos.push({src: vm.proceduresOveriew[0].video[n].main_url, desc: vm.proceduresOveriew[0].video[n].title});
                /*$scope.videosthumbs.push({src: vm.proceduresOveriew[0].video[n].thumbnail_url, desc: vm.proceduresOveriew[0].video[n].title});*/
            }
            

            // initial image index
            $scope._Index = vm.index;

            // if a current image is the same as requested image
            $scope.isActive = function (index) {

                return $scope._Index === index;
            };

            // show prev image
            $scope.showPrev = function () {
                alert("prev");
                $scope._Index = ($scope._Index > 0) ? --$scope._Index : $scope.videos.length - 1;
            };

            // show next image
            $scope.showNext = function () {
                alert("next");
                $scope._Index = ($scope._Index < $scope.videos.length - 1) ? ++$scope._Index : 0;
            };

            // show a certain image
            $scope.showPhoto = function (index) {
                $scope._Index = index;
            };

          $scope.answer = function(answer) {
            $mdDialog.hide(answer);
          };

        }

        //****** Check Procedure already fill or not*******
        if(CacheService.isCache("myProcedureList"))
        {
            var myProcedureList=CacheService.getCache("myProcedureList");
            for(var m=0; m<myProcedureList.length; m++)
            {
                if(myProcedureList[m].procedure==vm.title)
                {
                    vm.isStarFill=true;
                    vm.myProcedureId=myProcedureList[m].id;
                }
            }
        }

        //********For Manually Path Copy for Share into Share Button******
        vm.pathCopyForShare=function(isSuccess)
        {
            if(isSuccess)
            {
                $scope.alerts[0]={ type: 'success', msg: 'Data Copy into Clipboard' };
            }
            else
            {
                $scope.alerts[0]={ type: 'danger', msg: 'Browser not support System Clipboard. Copy link Manually' };
            }
        }
        
        ScrollService.scrollStart();
    }
    
    
})();

