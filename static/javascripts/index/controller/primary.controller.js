/**
 * Created by IDCS12 on 3/18/2015.
 */
(function(){
    angular.module('myApp.primary.controller')
        .controller('PrimaryIndexController', PrimaryIndexControllerFun);
    
    PrimaryIndexControllerFun.$inject = ['$scope', '$interval', '$mdDialog'];
    

    function PrimaryIndexControllerFun($scope, $interval, $mdDialog)
    {
        var vm=this;


        vm.type ="";
        vm.placeholder= "";
        vm.content = ['  Find Doctors, Symptoms, Conditions, Procedures, Medicines etc','  Get information of any symptoms by body part and their solution'];
        var i =0;
        var j = 0;
        vm.stopAndErasetype = function(){
            $interval.cancel(vm.timer);
            vm.placeholder="";
        };
        vm.starttype = function(){
            i = 0;
           vm.timer =$interval(function(){
            if (i < vm.content[j].length){
                vm.placeholder += vm.content[j][i];
            }else{
                vm.placeholder="";

                j++;
                i= 0;
                if(j >= vm.content.length){
                    j = 0;
                }
            }
            i++;
        },75);
        };
        vm.starttype();

        $mdDialog.show({
              controller: function($scope){

                $scope.temp_user=[{
                    name : 'vipul',
                    password : 'vipul123'
                },{
                    name : 'admin',
                    password : 'admin123'
                }];
                $scope.tempLoginClick=function()
                {
                    angular.forEach($scope.temp_user, function(key, value){
                        if($scope.temp_emial==key.name && $scope.temp_pass==key.password)
                        {
                            $mdDialog.hide();
                        }
                    });
                }
              },
              templateUrl: $scope.static_path +'templates/imgvideodialog/showBeforePresentation.html',
              parent: angular.element(document.body),
              clickOutsideToClose:true
            })
            .then(function(answer) {
              $scope.status = 'You said the information was "' + answer + '".';
            }, function() {
              $scope.status = 'You cancelled the dialog.';
        });
    }

})();

