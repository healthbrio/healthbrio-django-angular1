/**
 * Created by IDCS12 on 6/12/2015.
 */
(function(){
    'use strict';

    angular.module('myApp.search.controller')
        .controller('SecondarySearchController', SecondarySearchControllerFun);

    SecondarySearchControllerFun.$inject = ['$scope','$state', '$http', '$stateParams', '$interval', '$timeout', '$location', '$window', 'MainSearchService', 'ScrollService', 'CacheService'];


    function SecondarySearchControllerFun($scope, $state, $http, $stateParams, $interval, $timeout, $location, $window, MainSearchService, ScrollService, CacheService){
        var vm = this;
        vm.search_value=$stateParams.search_text;
        vm.isLoading=false;
        vm.mainSearchList=[];
        vm.searchList=[];
        vm.tapindex=0;
        vm.showHotSearchDiv = false;
        vm.windowHeight = $window.innerHeight;
        vm.windowHeight = vm.windowHeight - 171;

        vm.viewheight = {
            'height': vm.windowHeight + "px"
        };
        if($stateParams.search_text!=null)
        {
            $timeout(function(){
                searchFun($stateParams.search_text);
            }, 1500);
        }
        function searchFun(search_params)
        {
            vm.isLoading=true;
            vm.mainSearchList=[];
            if(CacheService.isCache("search_"+search_params))
            {
                vm.mainSearchList=CacheService.getCache("search_"+search_params);
                vm.searchList=vm.mainSearchList.Symptoms;
                vm.isLoading=false;
            }
            else
            {
                MainSearchService.all(search_params).success(function(response){
                    vm.mainSearchList=response;
                    vm.searchList=vm.mainSearchList.Symptoms;
                    vm.isLoading=false;
                    CacheService.setCache("search_"+search_params, vm.mainSearchList);
                }).error(function(error){
                    $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
                    vm.isLoading=false;
                });
            }
        }

        vm.fullSearch_click=function()
        {
            $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
            searchFun(vm.search_value);
            vm.tapindex=0;
            //$state.go("secondarysearch", {search_text : vm.search_value});
        }
        var search_interval_clear;
        vm.changeTap=function(tapindex)
        {
            ScrollService.scrollTop(".symptoms-bar_blank_div");
            vm.tapindex=tapindex;
            vm.searchList=[];
            var count_n=0;
            $interval.cancel(search_interval_clear);
            switch(tapindex)
            {
                case 0:
                    if(vm.mainSearchList.Symptoms.length!=0)
                    {
                        search_interval_clear=$interval(function(){
                            vm.searchList.push(vm.mainSearchList.Symptoms[count_n++]);
                            if(vm.searchList.length==vm.mainSearchList.Symptoms.length)
                            {
                                $interval.cancel(search_interval_clear);
                            }
                        });
                    }
                break;
                case 1:
                    if(vm.mainSearchList.condition.length!=0)
                    {
                        search_interval_clear=$interval(function(){
                            vm.searchList.push(vm.mainSearchList.condition[count_n++]);
                            if(vm.searchList.length==vm.mainSearchList.condition.length)
                            {
                                $interval.cancel(search_interval_clear);
                            }
                        });
                    }
                break;
                case 2:
                    if(vm.mainSearchList.Medications.length!=0)
                    {
                        search_interval_clear=$interval(function(){
                            vm.searchList.push(vm.mainSearchList.Medications[count_n++]);
                            if(vm.searchList.length==vm.mainSearchList.Medications.length)
                            {
                                $interval.cancel(search_interval_clear);
                            }
                        });
                    }
                break;
                case 3:
                    if(vm.mainSearchList.Procedures.length!=0)
                    {
                        search_interval_clear=$interval(function(){
                            vm.searchList.push(vm.mainSearchList.Procedures[count_n++]);
                            if(vm.searchList.length==vm.mainSearchList.Procedures.length)
                            {
                                $interval.cancel(search_interval_clear);
                            }
                        });
                    }
                break;
                case 4:
                    if(vm.mainSearchList.Speciality.length!=0)
                    {
                        search_interval_clear=$interval(function(){
                            vm.searchList.push(vm.mainSearchList.Speciality[count_n++]);
                            if(vm.searchList.length==vm.mainSearchList.Speciality.length)
                            {
                                $interval.cancel(search_interval_clear);
                            }
                        });
                    }
                break;
                case 5:
                    if(vm.mainSearchList.Speciality.length!=0)
                    {
                        search_interval_clear=$interval(function(){
                            vm.searchList.push(vm.mainSearchList.Speciality[count_n++]);
                            if(vm.searchList.length==vm.mainSearchList.Speciality.length)
                            {
                                $interval.cancel(search_interval_clear);
                            }
                        });
                    }
                break;
            }
        }
        
        vm.closeView=function(){
            $state.go("home");
            ScrollService.scrollBack();
        };
        ScrollService.scrollBegin();

    }
})();