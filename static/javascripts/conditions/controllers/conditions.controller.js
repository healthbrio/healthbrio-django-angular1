/**
 * Created by IDCS12 on 3/18/2015.
 */
(function(){
    angular.module('myApp.conditions.controller')
        .controller('ConditionsController1', ConditionsControllerFun);
    /*angular.module('myApp.conditions.controller')
        .controller('conditions_overview_Controller', ConditionsOverviewControllerFun);*/

    ConditionsControllerFun.$inject = ['$scope', '$state', '$http', '$interval', '$timeout', '$window', 'ConditionsService', 'ScrollService', 'CacheService'];
    //ConditionsOverviewControllerFun.$inject = ['$scope','$http', '$stateParams', '$state', 'ConditionsOverviewService', 'ScrollService', 'CacheService'];
    

    function ConditionsControllerFun($scope, $state, $http, $interval, $timeout, $window, ConditionsService, ScrollService, CacheService)
    {
        var vm=this;
        vm.conditionList = [];
        
        vm.isSearch_box = false;
        vm.isDisabled=false;
        vm.isLoading=false;

        vm.busy = false;

        vm.after = 0;

        vm.countover = true;
        var isloaded=true;
        vm.isFilter=false;
        vm.windowHeight = $window.innerHeight;
        vm.windowHeight = vm.windowHeight - 171 - 65;

        vm.viewheight = {
            'height': vm.windowHeight + "px"
        };



        /*if (CacheService.isCache("conditions_cache")) {
            var data = CacheService.getCache("conditions_cache");
            
            isloaded=false;
            var i=0;
            vm.isLoading=true;

            $timeout(function(){
                vm.isLoading=false;
                var interval=$interval(function(){

                    vm.conditionList.push(data.list[i]);
                    i++;
                    
                    if(i==data.list.length)
                    {
                        isloaded=true;
                        $interval.cancel(interval);
                    }
                
                }, 10);
            }, 1500);
            
            vm.after = data.after;
            window.isRefresh = false;
            vm.busy = false;
        }*/
         
        vm.loadMore = function () {

            if (CacheService.isCache("conditions_cache")) {
                var data = CacheService.getCache("conditions_cache");
                
                if(data.after>vm.after)
                {
                    isloaded=false;
                    for(var i=vm.after; i<(vm.after+20); i++)
                    {
                        if(data.list[i]!=null)
                        {
                            vm.conditionList.push(data.list[i]);
                        }
                    }
                    vm.after+=20;
                    if(data.after<=vm.after)
                    {
                        vm.after = data.after;
                        isloaded=true;
                    }

                    vm.busy = false;
                }
                if(vm.after>data.total)
                {
                    vm.countover=false;
                }
            }
            
            if (vm.busy || !isloaded) return;
            if(vm.isFilter) return;
            //var url = "http://52.74.163.60/api/symptoms_list/?offset="+vm.after;
            if (vm.countover) {
                vm.busy = true;
                ConditionsService.all(vm.after).success(function(data){
                    var items = data.results;
                    for (var i = 0; i < items.length; i++) {
                        vm.conditionList.push(items[i]);
                    }
                   
                    vm.after = vm.after + 20;
                    var data1 = {
                        list: vm.conditionList,
                        after: vm.after,
                        total : data.count 
                    };
                    
                    CacheService.setCache("conditions_cache", data1);

                    vm.busy = false;
                    console.log("after value is " + vm.after);
                    if (vm.after >= data.count) {
                        console.log("false countover");
                        vm.countover = false;
                    } else {
                        console.log("true countover ");
                    }
                }.bind(this)).error(function () {
                    vm.busy = false;
                    $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
                });
            }
        };

        vm.searchButton = function () {
             vm.isSearch_box = !vm.isSearch_box;
        };
        
        var timeout, interval;
        vm.filterListChange=function(filterList)
        {

            vm.isFilter=true;
            vm.conditionList=[];

            if(filterList!="")
            {
                
                $timeout.cancel(timeout);
                $interval.cancel(interval);
                vm.isLoading=true;
                timeout=$timeout(function(){
                    vm.isDisabled=true;
                    vm.isFilter=true;
                    ConditionsService.filterService(filterList).success(function (response) {
                        vm.conditionList=response.results;
                        vm.isDisabled=false;
                        vm.isLoading=false;
                    }).error(function(error){
                        $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
                        vm.isDisabled=false;
                        vm.isLoading=false;
                    });
                }, 500);
            }

            if(filterList=="")
            {

                $timeout.cancel(timeout);
                vm.isLoading=false;
                if (CacheService.isCache("conditions_cache")) {
                var data = CacheService.getCache("conditions_cache");
                isloaded=false;
                var i=0;
                interval=$interval(function(){

                    vm.conditionList.push(data.list[i]);
                    i++;
                    
                    if(i==data.list.length)
                    {
                        isloaded=true;
                        vm.isFilter=false;
                        $interval.cancel(interval);
                    }
                    
                }, 10);
                vm.after = data.after;

                }
            }
        }

        ScrollService.scrollBegin();
    }

    /*function ConditionsOverviewControllerFun($scope, $http, $stateParams, $state, ConditionsOverviewService, ScrollService, CacheService)
    {
        var vm=this;
        vm.title=$stateParams.condition_name;
        vm.overviewList = [];
        if(CacheService.isCache("on_refresh_condition_cache") && window.isRefresh)
        {
            vm.overviewList = CacheService.getCache("on_refresh_condition_cache");
        }
        else
        {
            ConditionsOverviewService.all().success(function(response){
                vm.overviewList = response;
                CacheService.setCache("on_refresh_condition_cache", response);
                }).error(function(){
                    alert("error")
            });
        }
        vm.closeView=function(){
            
            $state.go("conditions");
            CacheService.removeCache("on_refresh_condition_cache");
            ScrollService.scrollBack();
        }
        
        ScrollService.scrollStart();
    }*/
    
})();

