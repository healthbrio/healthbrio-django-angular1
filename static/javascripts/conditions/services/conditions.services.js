/**
 * Created by IDCS12 on 3/19/2015.
 */
(function(){
    'use strict';

    var condition_service=angular.module('myApp.conditions.services');
    condition_service.factory('ConditionsService', conditionsServiceFun);
    condition_service.factory('ConditionsOverviewService', conditionsOverviewServiceFun);
    conditionsServiceFun.$inject=['HttpService'];
    conditionsOverviewServiceFun.$inject=['HttpService'];
    
    function conditionsServiceFun(HttpService)
    {
        var conditionApiSerivce  = {
                all : all,
                filterService : filterService
            }

            function all(offsetValue){
                
                return (HttpService.PublicServiceURL('api/conditions/?offset='+offsetValue));

            }

            function filterService(filterText){
                
                return (HttpService.PublicServiceURL('api/conditions/filter/'+filterText+"?limit=5000"));
            }

        return conditionApiSerivce;
    }

    function conditionsOverviewServiceFun(HttpService)
    {
        var conditionOverviewApiSerivce  = {
                all : all
            }

            function all(){
                return (HttpService.PublicServiceURL('javascripts/conditions/jsondata/conditions.json'));     

            }

        return conditionOverviewApiSerivce;
    }
})();