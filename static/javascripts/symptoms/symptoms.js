/**
 * Created by IDCS12 on 3/18/2015.
 */
(function(){
    'use strict';

    angular.module('myApp.symptoms',[
        'myApp.symptoms.controller',
        'myApp.symptoms.directives',
        'myApp.symptoms.services'
    ]);


    //console.log("symptom getter");
    angular.module('myApp.symptoms.controller',[]);

    angular.module('myApp.symptoms.directives',[]);

    angular.module('myApp.symptoms.services',[]);
})();