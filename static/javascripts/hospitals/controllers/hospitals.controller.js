/**
 * Created by IDCS12 on 3/18/2015.
 */
(function(){
    angular.module('myApp.hospitals.controller')
        .controller('HospitalsController', HospitalsControllerFun);
	

    HospitalsControllerFun.$inject = ['$scope','$window', 'HospitalsService', 'ScrollService', 'CacheService'];

    function HospitalsControllerFun($scope, $window, HospitalsService, ScrollService, CacheService)
    {
        
        var vm=this;
        vm.hospitalList=[];
        vm.isLoading=true;
        vm.windowHeight = $window.innerHeight;
        vm.windowHeight = vm.windowHeight - 171;

        vm.viewheight = {
            'height': vm.windowHeight + "px"
        };
        HospitalsService.all().success(function(response){
            vm.isLoading=false;
            vm.hospitalList = response;
          }).error(function(){
                vm.isLoading=false;
                $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
        });

        ScrollService.scrollBegin();
    }
    
    
})();

