/**
 * Created by IDCS12 on 3/19/2015.
 */
(function(){
    'use strict';

    var hospital_service=angular.module('myApp.hospitals.services');
    hospital_service.factory('HospitalsService', HospitalsServiceFun);
    HospitalsServiceFun.$inject=['HttpService'];
    
    function HospitalsServiceFun(HttpService)
    {
        var hospitalApiSerivce  = {
            all : all
        }

        function all(){
            return (HttpService.PublicServiceURL('api/facility/'));
        }

        return hospitalApiSerivce;
    }

})();