/**
 * Created by IDCS12 on 3/19/2015.
 */
(function(){
    'use strict';

    var scroll_service=angular.module('myApp.scrollService', []);
    scroll_service.service('ScrollService', ScrollServiceFun);
    ScrollServiceFun.$inject=['$timeout'];
    function ScrollServiceFun($timeout)
    {
        
        this.scrollStart=function(){
            if(window.outerWidth<500)
            {
                $(".scroller").delay(500).animate({scrollLeft:'+='+(window.outerWidth+35)}, 500);
            }
            else
            {
                $(".scroller").delay(500).animate({scrollLeft:'+='+(window.outerWidth)}, 500);
            }
        };
        this.scrollBack=function(){
            
            if(window.outerWidth<500)
            {
                $(".scroller").delay(500).animate({scrollLeft:'-='+(window.outerWidth+35)}, 500);
            }
            else
            {
                $(".scroller").delay(500).animate({scrollLeft:'-='+((window.outerWidth/3))}, 500);
            }
        };
        this.scrollBegin=function(){
            $(".scroller").animate({scrollLeft:0}, 200);
        };
        
        this.scrollBackword=function(){
            if(window.outerWidth<500)
            {
                $(".scroller").animate({scrollLeft:'+='+(window.outerWidth+35)}, 500);
            }
            else
            {
                $(".scroller").animate({scrollLeft:'+='+(window.outerWidth/3)}, 500);
            }
        };

        this.scrollTop=function(class_){
            $(class_).animate({scrollTop:0}, 500);
        }
    }

})();