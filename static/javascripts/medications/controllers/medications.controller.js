/**
 * Created by IDCS12 on 3/18/2015.
 */
(function(){
    angular.module('myApp.medications.controller')
        .controller('MedicationsController', MedicationsControllerFun);
	angular.module('myApp.medications.controller')
        .controller('Medications_overview_Controller', MedicationsOverviewControllerFun);

    MedicationsControllerFun.$inject = ['$scope', '$state', '$http', '$interval', '$timeout', '$window','MedicationsService', 'ScrollService', 'CacheService'];
	MedicationsOverviewControllerFun.$inject = ['$scope','$http', '$stateParams', '$state', '$window', 'MedicationsOverview', 'ScrollService', 'CacheService'];

    function MedicationsControllerFun($scope, $state, $http, $interval, $timeout, $window, MedicationsService, ScrollService, CacheService)
    {
        var vm=this;
        vm.medicationList=[];
        vm.isSearch_box = false;

        vm.isDisabled=false;

        vm.isLoading=false;

        vm.busy = false;

        vm.after = 0;

        vm.countover = true;

        var isloaded=true;

        vm.isFilter=false;
        vm.windowHeight = $window.innerHeight;
        vm.windowHeight = vm.windowHeight - 171 - 65;

        vm.viewheight = {
            'height': vm.windowHeight + "px"
        };

        /*if (CacheService.isCache("medications_cache")) {

            var data = CacheService.getCache("medications_cache");
            isloaded=false;
            var i=0;
            vm.isLoading=true;

            $timeout(function(){
                vm.isLoading=false;
                var interval=$interval(function(){

                    vm.medicationList.push(data.list[i]);
                    i++;
                    
                    if(i==data.list.length)
                    {
                        isloaded=true;
                        $interval.cancel(interval);
                    }
                    
                }, 10);
            }, 1500);
                window.isRefresh==false;
                vm.after = data.after;
                vm.busy = false;
        }*/
        
        
        vm.loadMore = function () {
            
            if (CacheService.isCache("medications_cache")) {
                var data = CacheService.getCache("medications_cache");
                
                if(data.after>vm.after)
                {
                    isloaded=false;
                    for(var i=vm.after; i<(vm.after+20); i++)
                    {
                        if(data.list[i]!=null)
                        {
                            vm.medicationList.push(data.list[i]);
                        }
                    }
                    vm.after+=20;
                    if(data.after<=vm.after)
                    {
                        vm.after = data.after;
                        isloaded=true;
                    }
                    vm.busy = false;
                }
                if(vm.after>data.total)
                {
                    vm.countover=false;
                }
            }

            if (vm.busy || !isloaded) return;
            if(vm.isFilter) return;
            
            //var url = "http://52.74.163.60/api/symptoms_list/?offset="+vm.after;
            if (vm.countover) {
                vm.busy = true;
                MedicationsService.all(vm.after).success(function (data) {
                    var items = data.results;

                    for (var i = 0; i < items.length; i++) {

                        vm.medicationList.push(items[i]);
                    }
                    vm.after = vm.after + 20;
                    var data1 = {
                        list: vm.medicationList,
                        after: vm.after,
                        total : data.count
                    };
                    CacheService.setCache("medications_cache", data1);

                    vm.busy = false;
                    console.log("after value is " + vm.after);
                    if (vm.after >= data.count) {
                        console.log("false countover");
                        vm.countover = false;
                    } else {
                        console.log("true countover ");
                    }
                }.bind(this)).error(function () {
                    vm.busy = false;
                    $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
                });

            }
        }

        vm.searchButton = function () {
                vm.isSearch_box = !vm.isSearch_box;
            }
 
        var timeout, interval;
        vm.filterListChange=function(filterList)
        {
            
            vm.isFilter=true;
            vm.medicationList=[];

            if(filterList!="")
            {
                $timeout.cancel(timeout);
                $interval.cancel(interval);
                vm.isLoading=true;
                timeout=$timeout(function(){
                    vm.isDisabled=true;
                    vm.isFilter=true;
                    MedicationsService.filterService(filterList).success(function (response) {
                        vm.medicationList=response.results;
                        vm.isDisabled=false;
                        vm.isLoading=false;
                    }).error(function(error){
                        $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
                        vm.isDisabled=false;
                        vm.isLoading=false;
                    });
                }, 500);
            }

            if(filterList=="")
            {

                $timeout.cancel(timeout);
                vm.isLoading=false;
                if (CacheService.isCache("medications_cache")) {
                var data = CacheService.getCache("medications_cache");
                isloaded=false;
                var i=0;
                interval=$interval(function(){

                    vm.medicationList.push(data.list[i]);
                    i++;
                    
                    if(i==data.list.length)
                    {
                        isloaded=true;
                        vm.isFilter=false;
                        $interval.cancel(interval);
                    }
                    
                }, 10);
                vm.after = data.after;

                }
            }
        }
        
        ScrollService.scrollBegin();
    }
    
    function MedicationsOverviewControllerFun($scope, $http, $stateParams, $state, $window, MedicationsOverview, ScrollService, CacheService)
    {
        
        //alert("MedicationsControllerFun");
        var vm=this;
        vm.medicationOveriew=[];
        vm.isLoading=true;
        vm.myMedicationId=0;
        vm.isStarFill=false;
        
        vm.viewheight = {
            height : ($window.innerHeight-171)+"px"
        }

        $scope.whereCouldGoData = [];

        if(CacheService.isCache("medications_"+$stateParams.medication_id))
        {
            //***Search sessionStorage when click any Cause
            vm.medicationOveriew=CacheService.getCache("medications_"+$stateParams.medication_id);
            vm.isLoading=false;

            vm.title=vm.medicationOveriew[0].name;

            //********* For Where Could Go Panel**********
            $scope.whereCouldGoData = [{
                relate_specialties : "Find Doctors Relate to this Medication"
            },
            {
                relate_hospitals : "Find Hospitals, Urgent Care etc. Relate to this Medication"
            }];
        }
        else
        {
            //alert("from api");
            //***get Data from API and store it sessionStorage
            MedicationsOverview.all($stateParams).success(function(response){
                vm.medicationOveriew = response;
                if(vm.medicationOveriew[0].name!==$stateParams.medication_name)
                {
                    vm.medicationOveriew[0].name=$stateParams.medication_name+" ("+vm.medicationOveriew[0].name+")";
                    vm.title=vm.medicationOveriew[0].name;
                }
                else
                {
                    vm.title=vm.medicationOveriew[0].name;
                }
                
                CacheService.setCache("medications_"+$stateParams.medication_id, vm.medicationOveriew);
                vm.isLoading=false;

                //********* For Where Could Go Panel**********
                $scope.whereCouldGoData = [{
                    relate_specialties : "Find Doctors Relate to this Medication"
                },
                {
                    relate_hospitals : "Find Hospitals, Urgent Care etc. Relate to this Medication"
                }];

              }).error(function(error){
                    $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
            });
        }

        vm.closeView=function(){
            
            $state.go($state.current.name.substring(0, $state.current.name.indexOf(".medications_overview")));
            ScrollService.scrollBack();
        }

        //****** Check Medication already fill or not*******
        if(CacheService.isCache("myMedicationList"))
        {
            var myMedicationList=CacheService.getCache("myMedicationList");
            for(var m=0; m<myMedicationList.length; m++)
            {
                if(myMedicationList[m].salt==vm.title)
                {
                    vm.isStarFill=true;
                    vm.myMedicationId=myMedicationList[m].id;
                }
            }

        }

        //********For Manually Path Copy for Share into Share Button******
        vm.pathCopyForShare=function(isSuccess)
        {
            if(isSuccess)
            {
                $scope.alerts[0]={ type: 'success', msg: 'Data Copy into Clipboard' };
            }
            else
            {
                $scope.alerts[0]={ type: 'danger', msg: 'Browser not support System Clipboard. Copy link Manually' };
            }
        }
        ScrollService.scrollStart();
    }
    
})();

