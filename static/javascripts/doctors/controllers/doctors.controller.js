/**
 * Created by IDCS12 on 3/18/2015.
 */
(function(){
    angular.module('myApp.doctors.controller')
        .controller('DoctorsController', DoctorsControllerFun);
	angular.module('myApp.doctors.controller')
        .controller('DoctorsOverviewController', DoctorsOverviewControllerFun);
    angular.module('myApp.doctors.controller')
        .controller('DoctorsDetailsController', DoctorsDetailsControllerFun);



    DoctorsControllerFun.$inject = ['$scope', '$rootScope', '$state', '$stateParams', '$http', '$interval', '$timeout', '$window', 'DoctorsService', 'ScrollService', 'CacheService', 'GeoLocationService'];
	DoctorsOverviewControllerFun.$inject = ['$scope','$http', '$stateParams', '$state', '$timeout', '$window', 'ScrollService', 'CacheService', 'DoctorsServiceList'];
    DoctorsDetailsControllerFun.$inject = ['$scope','$http', '$stateParams', '$state', 'ScrollService', 'DoctorsDetailsList'];

    function DoctorsControllerFun($scope, $rootScope, $state, $stateParams, $http, $interval, $timeout, $window, DoctorsService, ScrollService, CacheService, GeoLocationService)
    {

        var vm=this;
        vm.specilityList=[];

        vm.isSearch_box = false;

        vm.isDisabled=false;

        vm.isLoading=true;
        vm.isMapLoading=false;

        vm.busy = false;

        vm.after = 1;

        vm.countover = true;

        var isloaded=true;

        vm.isFilter=false;
        vm.windowHeight = $window.innerHeight - 174 - 65;
        
        vm.viewheight = {
            'height': vm.windowHeight + "px"
        };
        
        if(CacheService.isCache("user_location"))
        {
            $rootScope.isUserLocationFind=true;
            var location_={
                city : CacheService.getCache("user_location").user_city_slug,
                location : CacheService.getCache("user_location").user_location_slug
            }

            if (CacheService.isCache("speciality_cache")) {

                vm.specilityList=[];
                var data = CacheService.getCache("speciality_cache");
                isloaded=false;
                var i=0;
                vm.isLoading=true;

                $timeout(function(){
                    vm.isLoading=false;
                    var interval=$interval(function(){

                        vm.specilityList.push(data.list[i]);
                        i++;
                        
                        if(i==data.list.length)
                        {
                            isloaded=true;
                            $interval.cancel(interval);
                        }
                        
                    }, 10);
                }, 1500);
                    window.isRefresh==false;
                    vm.after = data.after;
                    vm.busy = false;
            }

            vm.loadMore = function () {

                if (vm.busy || !isloaded) return;
                if(vm.isFilter) return;

                if (vm.countover) {
                    vm.busy = true;
                    DoctorsService.locationFind(location_, vm.after).success(function (response) {
                        
                        vm.isLoading=false;
                        var data = response[0].speciality;
                        for (var i = 0; i < data.length; i++) {

                            vm.specilityList.push(data[i]);
                        }

                        vm.after = vm.after + 1;

                        var data1 = {
                            list: vm.specilityList,
                            after: vm.after
                        };
                        //CacheService.setCache("speciality_cache", data1);

                        vm.busy = false;
                        console.log("after value is " + vm.after);
                        if (vm.after > response[1].last_page) {
                            console.log("false countover");
                            vm.countover = false;
                        } else {
                            console.log("true countover ");
                        }
                    }.bind(this)).error(function () {
                        vm.busy = false;
                        vm.isLoading=false;
                        $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
                    });

                }
            };

            vm.searchButton = function () {
                    vm.isSearch_box = !vm.isSearch_box;
                }
     
            var timeout, interval;
            vm.filterListChange=function(filterList)
            {
                
                vm.isFilter=true;
                vm.specilityList=[];

                if(filterList!="")
                {
                    $timeout.cancel(timeout);
                    $interval.cancel(interval);
                    vm.isLoading=true;
                    timeout=$timeout(function(){
                        vm.isDisabled=true;
                        vm.isFilter=true;
                        DoctorsService.filterService(filterList).success(function (response) {
                            vm.specilityList=response.results;
                            vm.isDisabled=false;
                            vm.isLoading=false;
                        }).error(function(error){
                            $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
                            vm.isDisabled=false;
                            vm.isLoading=false;
                        });
                    }, 500);
                }

                if(filterList=="")
                {

                    $timeout.cancel(timeout);
                    vm.isLoading=false;
                    if (CacheService.isCache("speciality_cache")) {
                    var data = CacheService.getCache("speciality_cache");
                    isloaded=false;
                    var i=0;
                    interval=$interval(function(){

                        vm.specilityList.push(data.list[i]);
                        i++;
                        
                        if(i==data.list.length)
                        {
                            isloaded=true;
                            vm.isFilter=false;
                            $interval.cancel(interval);
                        }
                        
                    }, 10);
                    vm.after = data.after;
                    }
                }
            }
        }

        vm.findMyCurrentLocation = function()
        {
            $scope.isUserLocationFind=!$scope.isUserLocationFind;
        }
        $scope.$on("LocationChangesEvent", function($event){
            $state.transitionTo($state.current, {doctors_params : $stateParams.doctors_params+1}, { reload: false, inherit: true, notify: true }); 
        });

        vm.closeView=function(){
            try{
                $state.go($state.current.name.substring(0, $state.current.name.indexOf(".specialityListFromWCIG")));
            }catch(error){
                $state.go("home");
            }

            ScrollService.scrollBack();
        };

        if($state.current.name === "doctors")
        {
            ScrollService.scrollBegin();
        }
        else
        {
            ScrollService.scrollStart();
        }
    }



    function DoctorsOverviewControllerFun($scope, $http, $stateParams, $state, $timeout, $window, ScrollService, CacheService, DoctorsServiceList)
    {
        var vm = this;
        vm.doctorsList=[];
        vm.locationList=[];

        vm.isLoading=true;
        vm.title="";
        vm.localitydiv =true;
        vm.isMouseOver=false;
        vm.isMouseClick=false;
        vm.doctors_map_array=new Array();

        vm.busy = false;

        vm.after = 1;

        vm.countover = true;

        var isloaded=true;

        vm.location_name = "";

        vm.windowHeight = $window.innerHeight;
        
        vm.viewheight = {
            'height': (vm.windowHeight - 171 - 10) + "px"
        };
        vm.mapHeight = {
            'height': (vm.windowHeight - 171 - 18) + "px"  
        }

        var location_={
            city : CacheService.getCache("user_location").user_city_slug,
            location : CacheService.getCache("user_location").user_location_slug
        }
        if(location_.location)
        {
            vm.location_name = location_.location+", "+location_.city;
        }
        else
        {
            vm.location_name = location_.city;
        }

        if(CacheService.isCache("speciality_"+$stateParams.specility_id))
        {
            vm.isLoading=false;
            vm.doctorsList=CacheService.getCache("speciality_"+$stateParams.specility_id);
            setGoogleMap();
        }
        else
        {
            vm.loadMore = function () {

                if (vm.busy || !isloaded) return;
                if(vm.isFilter) return;

                if (vm.countover) {
                    vm.busy = true;
                    DoctorsServiceList.all($stateParams.specility_id, location_, vm.after).success(function (response) {
                        
                        var data=response[0].doctor_detail;
                        
                        if(vm.after==1)
                        {
                            angular.forEach(response[1].location, function(key, value){
                                key.isChecked=false;
                                vm.locationList.push(key);
                            });
                            vm.title = response[2].title;
                        }
                        vm.isLoading=false;
                        
                        for (var i = 0; i < data.length; i++) {

                            vm.doctorsList.push(data[i]);
                        }

                        setGoogleMap();
                        vm.after = vm.after + 1;

                        vm.busy = false;
                        
                        if (vm.after > response[3].last_page) {
                            vm.countover = false;
                        }

                    }.bind(this)).error(function () {
                        vm.busy = false;
                        vm.isLoading=false;
                        $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
                    });

                }
            }
        }

        //**********add Doctors Maps Value******
        function setGoogleMap()
        {
            vm.doctors_map_array = [];
            angular.forEach(vm.doctorsList, function(key, value)
            {
                var latitude_value, longitude_value;
                angular.forEach(key.latitude, function(key1, value1){
                    latitude_value=key1;
                });
                angular.forEach(key.longitude, function(key1, value1){
                    longitude_value=key1;
                });

                var ll_arr={
                    latitude : latitude_value,
                    longitude : longitude_value,
                    doctor_name_for_map : key.name
                }

                if(latitude_value && longitude_value)
                {
                    vm.doctors_map_array.push(ll_arr);
                }
                vm.isMapLoading=true;
            });
        }
        
        vm.showLocalityData = function(){
            vm.localitydiv = vm.localitydiv ? false : true;
            $scope.isUserLocationFind=false;
        };

        vm.doctorMouseFocus = function(latitude_)
        {
            $scope.$broadcast("DoctorMouseFocusEvent", {latitude : latitude_});
        }
        vm.doctorMouseOut = function(){
            $scope.$broadcast("DoctorMouseOutEvent");
        }

        vm.localitySelect = function(location_list){
            var locality="";
            vm.isLoading=true;
            angular.forEach(location_list, function(key, value){
                if(key.isChecked)
                {
                    locality+=key.slug+",";
                }
            });

            DoctorsServiceList.doctorListFilterByLocation($stateParams.specility_id, location_, locality).success(function(response){
                vm.doctorsList=response[0].doctor_detail;
                vm.isLoading=false;
                setGoogleMap();
            }).error(function(error){
                $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
                vm.isLoading=false;
            });
        }

        vm.closeView=function(){
            
            $state.go($state.current.name.substring(0, $state.current.name.indexOf(".doctor_list")));
            ScrollService.scrollBack();
        };

        ScrollService.scrollStart();
    }


    function DoctorsDetailsControllerFun($scope, $http, $stateParams, $state, ScrollService, DoctorsDetailsList){
        var vm = this;
        vm.doctorsDetails=[];
        vm.summary = "";
        vm.isLoading=true;

        var showChar = 100;
        var ellipsestext = "...";
        var moretext = "more";
        var lesstext = "less";


        DoctorsDetailsList.all($stateParams.doctor_id).success(function(response){
            vm.doctorsDetails=response;
            /*var summary = vm.doctorsDetails[0].summary;

            alert(summary.length);
            if(summary.length   > showChar) {
                var c = summary.substr(0, showChar);
                var h = summary.substr(showChar-1, summary.length - showChar);
                vm.summary = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
                alert(vm.summary);

            }*/

            vm.isLoading=false;
        }).error(function(error){
            $scope.alerts[0]={ type: 'danger', msg: 'Oh snap! Connection Fail. TRY AGAIN' };
            vm.isLoading=false;
        });

        //alert(angular.element(document.getElementsByClassName('comment')[0]).children());
        //var summary = document.getElementsByClassName('avatar')[0];
        //var content = angular.element(summary).html();


        //alert(content);


        vm.closeView=function(){
            $state.go($state.current.name.substring(0, $state.current.name.indexOf(".overview")));
            ScrollService.scrollBack();
        };


        ScrollService.scrollStart();
    }
    
})();

