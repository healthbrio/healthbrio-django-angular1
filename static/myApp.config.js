(function () {
  'use strict';

  angular
    .module('myApp.config')
    .config(config);

  config.$inject = ['$locationProvider', 'ngClipProvider'];


   /** @name config
   * @desc Enable HTML5 routing*/

  function config($locationProvider, ngClipProvider) {

    ngClipProvider.setPath("bower_components/zeroclipboard-master/dist/ZeroClipboard.swf");

    $locationProvider.html5Mode({
      enabled: false,
      requireBase: false
    });
    $locationProvider.hashPrefix('!');
  }
})();
